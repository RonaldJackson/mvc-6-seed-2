using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using MVC6_Seed_Project.Models;

namespace MVC6_Seed_Project.Controllers
{
    public class CompanyContactsController : Controller
    {
        private ApplicationDbContext _context;

        public CompanyContactsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: CompanyContacts
        public IActionResult Index()
        {
            var applicationDbContext = _context.CompanyContacts.Include(c => c.Company);
            return View(applicationDbContext.ToList());
        }

        // GET: CompanyContacts/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CompanyContacts companyContacts = _context.CompanyContacts.Single(m => m.ContactID == id);
            if (companyContacts == null)
            {
                return HttpNotFound();
            }

            return View(companyContacts);
        }

        // GET: CompanyContacts/Create
        public IActionResult Create()
        {
            ViewData["CompanyID"] = new SelectList(_context.Company, "CompanyID", "Company");
            return View();
        }

        // POST: CompanyContacts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CompanyContacts companyContacts)
        {
            if (ModelState.IsValid)
            {
                _context.CompanyContacts.Add(companyContacts);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CompanyID"] = new SelectList(_context.Company, "CompanyID", "Company", companyContacts.CompanyID);
            return View(companyContacts);
        }

        // GET: CompanyContacts/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CompanyContacts companyContacts = _context.CompanyContacts.Single(m => m.ContactID == id);
            if (companyContacts == null)
            {
                return HttpNotFound();
            }
            ViewData["CompanyID"] = new SelectList(_context.Company, "CompanyID", "Company", companyContacts.CompanyID);
            return View(companyContacts);
        }

        // POST: CompanyContacts/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CompanyContacts companyContacts)
        {
            if (ModelState.IsValid)
            {
                _context.Update(companyContacts);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["CompanyID"] = new SelectList(_context.Company, "CompanyID", "Company", companyContacts.CompanyID);
            return View(companyContacts);
        }

        // GET: CompanyContacts/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            CompanyContacts companyContacts = _context.CompanyContacts.Single(m => m.ContactID == id);
            if (companyContacts == null)
            {
                return HttpNotFound();
            }

            return View(companyContacts);
        }

        // POST: CompanyContacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            CompanyContacts companyContacts = _context.CompanyContacts.Single(m => m.ContactID == id);
            _context.CompanyContacts.Remove(companyContacts);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
