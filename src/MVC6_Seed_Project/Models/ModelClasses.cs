﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MVC6_Seed_Project.Models
{
    public class Company
    {
        [Key]
        public int CompanyID { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Display(Name = "Company Registration Number")]
        public string CompRegNumber { get; set; }
        [Display(Name = "Vat Registered")]
        public bool VatRegistered { get; set; }
        [Display(Name = "VAT Number")]
        public string VATNumber { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Company Registration Date")]
        [DataType(DataType.Date)]
        public DateTime CompanyRegDate { get; set;
        }
        public virtual ICollection<CompanyContacts> Contacts { get; set; }
        public virtual ICollection<CompanyAddress> CompnayAddress { get; set; }
    }

    public class CompanyContacts
    {
        [Key]
        public int ContactID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateofBirth { get; set; }
        public string Gender { get; set; }
        public string Designation { get; set; }
        public string TelephoneNo { get; set; }
        public string CellphoneNo { get; set; }
        public int CompanyID { get; set; }
        public virtual Company Company { get; set; }

    }

    public class CompanyAddress
    {
        [Key]
        public int AddressID { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Suburb { get; set; }
        public string PostalCode { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public int CompanyID { get; set; }
        public virtual Company Company { get; set; }

    }
    public class Tenant
    { 
        public int TenantID { get; set; }
        public string TenantName { get; set; }
        public string Location { get; set; }
        public string Currency { get; set; }
        public string Language { get; set; }
        public string Status { get; set; }
        public DateTime InceptionDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int NoUsers { get; set; }
        public string LicenseType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile234 { get; set; }

    }

}
